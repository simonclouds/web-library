Citer
=====

This is a quick example, using components of the Zotero web library, to create formatted citations using your own citeproc-node instance rather than creating items through the Zotero API.

It uses the item creation and citation UI from the Zotero web library, but creates CSL JSON directly from the item and submits it to a citeproc-node server specified in the config file.
